#!/usr/bin/perl
use strict;
use autodie;
use Getopt::Long;
my $h=0;
GetOptions ('help' => \$h, 'h' => \$h );
if ($h==1 || $ARGV[0] eq ""){ # If asked for help or did not set up any argument
	print "# Script to get alpha diversity by group
# Argument 0 : null
\n";
	die "\n";
}

my $out_file="List_relevant_clusters";
my $ab_file="Viral_clusters_relative_abundance.csv";

open my $csv,"<",$ab_file;
my %store_coverage;
my %store_2;
my %sample_name;
my %check_sample;
my %sample_to_station;
my $tag=0;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tag==0){
		$tag=1;
		for (my $i=1;$i<=$#tab;$i++){
			$sample_name{$i}=$tab[$i];
			$check_sample{$tab[$i]}=1;
			my @t2=split("_",$tab[$i]);
			$sample_to_station{$tab[$i]}=$t2[0];
		}
	}
	else{
		my $cluster=$tab[0];
		for (my $i=1;$i<=$#tab;$i++){
			$store_coverage{$sample_name{$i}}{$cluster}+=$tab[$i];
			$store_2{$sample_name{$i}}{"total"}+=$tab[$i];
		}
	}
}
close $csv;

my %select;
foreach my $sample (sort keys %store_coverage){
	my $total=0;
	my $simpson=0;
	my $total_simpson=0;
	foreach my $cluster (keys %{$store_coverage{$sample}}){
		if ($store_coverage{$sample}{$cluster}>1){
			$simpson+=$store_coverage{$sample}{$cluster}*($store_coverage{$sample}{$cluster}-1);
			$total_simpson+=$store_coverage{$sample}{$cluster};
		}
	}
	my $den=($total_simpson*($total_simpson-1));
	$simpson/=$den;
	my $simpson_index=$simpson;
	my $simpson_diversity=1-$simpson;
	my $th=0.8*$simpson_index;
	my $th_2=$simpson_diversity+(0.1*(1-$simpson_diversity)); # accumulating up to 90% of the diversity 
	print "sample $sample -> Simpson index is $simpson -> th will be $th / simpson diversity index is $simpson_diversity, which would give the th $th_2\n";
	$simpson=0;
	foreach my $cluster (sort {$store_coverage{$sample}{$b}<=>$store_coverage{$sample}{$a}} keys %{$store_coverage{$sample}}){
		if ($cluster eq "unclustered"){next;}
		if ($store_coverage{$sample}{$cluster}>1){
			$simpson+=$store_coverage{$sample}{$cluster}*($store_coverage{$sample}{$cluster}-1);
			my $temp_s=$simpson/$den; # always using the same denominator
			my $temp_s_d=1-$temp_s;
			my $selected="";
			if ($temp_s<$th){
				if ($temp_s_d>$th_2){
					$selected="selected";
				}
				else{
					print "?????\n";
					<STDIN>;
				}
				if ($cluster=~/^unclustered/){
					print "\twe would like to select $cluster ? \n";
				}
				else{
					$select{$cluster}{$sample}=1;
				}
			}
			else{
			}
			print "$cluster - $simpson - $den - $store_coverage{$sample}{$cluster} - $temp_s - $temp_s_d - $selected\n";
		}
	}
}
print "Now the selection of abundant not just once\n";
print "##\n";
print "Cluster\tAll samples\tAb samples\tAll stations\tAb stations\n";
my $n_selected=0;
foreach my $cluster (keys %select){
	my $n_st=0;
	my $n_sa=0;
	my $total_st=0;
	my $total_sa=0;
	my %vu;
	foreach my $sample (keys %{$select{$cluster}}){
		if (!defined($vu{$sample_to_station{$sample}})){
			$n_st++;
		}
		$n_sa++;
		$vu{$sample_to_station{$sample}}=1;
	}
	%vu=();
	foreach my $sample (keys %store_coverage){
		if ($store_coverage{$sample}{$cluster}>0){
			$total_sa++;
			if (!defined($vu{$sample_to_station{$sample}})){
				$total_st++;
			}
			$vu{$sample_to_station{$sample}}=1;
		}
	}
	if ($n_st>1){
		$n_selected++;
		print "$cluster\t$total_sa\t$n_sa\t$total_st\t$n_st\n";
		foreach my $sample (keys %store_coverage){
			$store_2{$sample}{"selected"}+=$store_coverage{$sample}{$cluster};
		}
	}
}
print "\n\n";
print "$n_selected based on multiple detections\n";
print "## Relative abundance of these selected VCs: \n";
foreach my $sample (keys %store_coverage){
	my $r_1=$store_2{$sample}{"selected"}/$store_2{$sample}{"total"};
	print "$sample\t$r_1\n";
}