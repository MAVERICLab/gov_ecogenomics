#!/usr/bin/perl
use strict;
use autodie;
# Script to get rank abundance curve of clusters
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to get rank abundance curve of clusters
# Argument 0 : toto\n";
	die "\n"
}


my $in_file="Viral_clusters_relative_abundance.csv";
my $metadata="Metadata_samples_basic.tsv";
my $out_file="Cluster_ab.csv";


my %info;
open my $tsv,"<",$metadata;
my %col_name;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($_=~/^##/){
		for(my $i=1;$i<=$#tab;$i++){
			$col_name{$i}=$tab[$i];
		}
	}
	else{
		for (my $i=1;$i<=$#tab;$i++){
# 			if ($tab[$i] eq "DCM" || $tab[$i] eq "SUR"){$tab[$i]="EPI";}
			$info{$tab[0]}{$col_name{$i}}=$tab[$i];
		}
	}
}
close $tsv;

my @tab_sample=keys %info;

open(CSV,"<$in_file") || die ("pblm opening file $in_file\n");
my $tag=0;
my %sample_name;
my %check_sample;
my %store_coverage;
my %store_total;
my %origin_sample_to_seq;
while(<CSV>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tag==0){
		$tag=1;
		for (my $i=1;$i<=$#tab;$i++){
			$sample_name{$i}=$tab[$i];
			$check_sample{$tab[$i]}=1;
		}
	}
	else{
		my $cluster=$tab[0];
		for (my $i=1;$i<=$#tab;$i++){
			if ($cluster eq "unclustered"){}
			else{$store_coverage{$sample_name{$i}}{$cluster}+=$tab[$i];}
			$store_total{$sample_name{$i}}+=$tab[$i];
		}
	}
}
close CSV;

my $out_file_global="Rank_ab_clusters_all.csv";
open my $s1,">",$out_file_global;
print $s1 "Cluster,Sample,Longhurst,Biome,Ocean,Rank,Rel_ab,Depth\n";
foreach(sort keys %store_coverage){
	my $sample=$_;
	if ($info{$sample}{"Depth"} eq "BAT"){next;}
	my $longhurst=$info{$sample}{"Longhurst"};
	my $biome=$info{$sample}{"Biome"};
	my $ocean=$info{$sample}{"Ocean"};
	my $depth=$info{$sample}{"Depth"};
	print "... sample $sample \n";
	my @tab=sort { $store_coverage{$sample}{$b} <=> $store_coverage{$sample}{$a} } keys %{$store_coverage{$sample}};
	for (my $i=0;$i<=$#tab;$i++){
		my $rel_ab=sprintf("%0.4f",$store_coverage{$sample}{$tab[$i]}/$store_total{$sample}*100);
		print $s1 "$tab[$i],$sample,$longhurst,$biome,$ocean,$i,$rel_ab,$depth\n";
	}
}
close $s1;
