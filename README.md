# README #

### What is this repository for? ###

* Scripts used in the manuscript "Ecogenomics and biogeochemical impacts of globally abundant ocean viruses"
* Version 1.1 (2016-02-29)

### Folders ###

* Virus clusters prevalence: script used to compute rank abundance curves for virus clusters and select abundant VCs
* Host prediction: scripts used to assess hosts for viral sequences, and host range for viral clusters
* Host diversity: scripts used to calculate group-by-group alpha and beta diversity from miTAGs data, and compare it to the number of virus clusters associated with the group.

### Contacts ###

* roux . 8 at osu . edu
