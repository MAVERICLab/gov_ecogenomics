#!/usr/bin/perl
use strict;
# use warnings;
use autodie;
use Getopt::Long;
my $h=0;
GetOptions ('help' => \$h, 'h' => \$h );
if ($h==1 || $ARGV[0] eq ""){ # If asked for help or did not set up any argument
	print "# Script to get alpha diversity by group
# Argument 0 : toto
\n";
	die "\n";
}



my $in_file_ab="miTAG.taxonomic.profiles_counts.tsv";
my $in_file_afi="miTAG.taxonomic.profiles_affiliation.tsv";

my $out_file="Global_Schao_and_Sorensen.csv";
my $out_file_2="Alphadiv_Tara_MetaG.ggplot2";


my $input_list="Ordered_input_list.tsv"; 
# File with a list of group to be processed, e.g.
# Bacteria-Proteobacteria-Gammaproteobacteria
# Bacteria-Cyanobacteria
my %keep_group;
my %order;
my $i=0;
open my $tsv,"<",$input_list;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$order{$tab[0]}=$i;
	$keep_group{$tab[0]}=1;
	$i++;
}
close $tsv;


my %id_to_group;
my %count;
open my $tsv,"<",$in_file_afi;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[0] eq "OTU.rep"){}
	else{
		$id_to_group{$tab[0]}=$tab[1]."-".$tab[2];
		if ($tab[2] eq "Proteobacteria"){$id_to_group{$tab[0]}=$tab[1]."-".$tab[2]."-".$tab[3];}
		$id_to_group{$tab[0]}=~s/\s/_/g;
		if (defined($order{$id_to_group{$tab[0]}})){}
		else{
			$order{$id_to_group{$tab[0]}}=$i;
			$i++;
		}
	}
}
close $tsv;


my %store;
my %col_name;
my %store_metrics;
my %store_total;
open my $tsv,"<",$in_file_ab;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[0] eq "OTU.rep"){
 		for (my $i=1;$i<=$#tab;$i++){$col_name{$i}=$tab[$i];}
	}
	else{
		my $group=$id_to_group{$tab[0]};
		for (my $i=1;$i<=$#tab;$i++){
			$store{$group}{$tab[0]}{$col_name{$i}}+=$tab[$i];
			if ($tab[$i]>0){
				$store_metrics{$group}{$col_name{$i}}{"nb_otus"}++;
				$store_metrics{$group}{$col_name{$i}}{"rel_ab"}+=$tab[$i];
				$store_total{$col_name{$i}}+=$tab[$i];
			}
		}
	}
}
close $tsv;

foreach my $group (keys %store_metrics){
	foreach my $sample (keys %{$store_metrics{$group}}){
		$store_metrics{$group}{$sample}{"rel_ab"}/=$store_total{$sample};
	}
}

my @tab_samples=sort values %col_name;

my %store_sorensen;
my %store_schao;
my %store_relab;
my $store_total=0;
my $out_file_tmp="Group_OTUs.tsv";
my $out_file_tmp_sorensen="Group_OTUs_nonzerosamples.tsv";
my @tab_groups=sort keys %store;
my $store_mat="Group,Schao_avg,Schao_sd,Shannon_avg,Shannon_sd\n";
foreach my $group (@tab_groups){
	print "Counting group $group\n";
	open my $s1,">",$out_file_tmp;
	open my $s2,">",$out_file_tmp_sorensen;
	my @tab_species=sort keys %{$store{$group}};
	my $f_l=join(",",@tab_species);
	print $s1 "$f_l\n";
	print $s2 "$f_l\n";
	my $total_epi=0;
	foreach(@tab_samples){
		my $sample=$_;
		my @t2=split("_",$sample);
		my $type="MES";
		if ($t2[1] eq "DCM" || $t2[1] eq "SUR" || $t2[1] eq "MIX" || $t2[1] eq "dDCM"){$type="EPI";}
		if ($type eq "MES"){print "$sample is considered a MES\n";}
		if ($type eq "EPI"){
			my $l=$sample;
			my $total=0;
			foreach(@tab_species){
				if (defined($store{$group}{$_}{$sample})){
					$l.=",".$store{$group}{$_}{$sample};
					$total+=$store{$group}{$_}{$sample};
					$store_relab{$group}+=$store{$group}{$_}{$sample};
					$store_total+=$store{$group}{$_}{$sample};
					$total_epi+=$store{$group}{$_}{$sample};
				}
				else{
					$l.=",0";
				}
			}
			if ($total>0){
				print $s2 "$l\n";
			}
			print $s1 "$l\n";
		}
	}
	close $s1;
	close $s2;
	if ($total_epi==0){
		print "No OTU of $group in epipelagic sapmles, moving on\n";
		next;
	}
	print "file ready for R\n";
	my $out=`Rscript alpha_div.R $group $out_file_tmp $out_file_tmp_sorensen`;
	print "out - $out\n";
	open my $csv,"<","sorensen.csv";
	while(<$csv>){
		chomp($_);
		$_=~s/\"//g;
		my @tab=split(",",$_);
		if ($tab[0] eq ""){}
		else{
			$store_sorensen{$group}=$tab[1];
		}
	}
	close $csv;
	open my $csv,"<","chao_global.csv";
	while(<$csv>){
		chomp($_);
		$_=~s/\"//g;
		my @tab=split(",",$_);
		if ($tab[0] eq ""){}
		else{
			$store_schao{$group}=$tab[1];
		}
	}
	close $csv;
	open my $csv,"<","chao.csv";
	while(<$csv>){
		chomp($_);
		$_=~s/\"//g;
		my @tab=split(",",$_);
		if ($tab[0] eq ""){}
		else{
			$store_metrics{$group}{$tab[0]}{"Schao"}=$tab[1];
		}
	}
	close $csv;
	open my $csv,"<","shannon.csv";
	while(<$csv>){
		chomp($_);
		$_=~s/\"//g;
		my @tab=split(",",$_);
		if ($tab[0] eq ""){}
		else{
			$store_metrics{$group}{$tab[0]}{"Shannon"}=$tab[1];
		}
	}
	print "$group done -> $store_sorensen{$group} / $store_schao{$group}\n";
}
print "$store_mat\n";

open my $s1,">",$out_file;
print $s1 "Group,Group_d,Relab,Sorensen,Schao,Type\n";
foreach my $group (sort { $order{$a} <=> $order{$b} } keys %store_sorensen){
	my $group_display=$group;
	if ($keep_group{$group}==1){}
	else{$group_display="Other";}
	my $relab=$store_relab{$group}/$store_total;
	my $type="rare";
	if ($relab>0.01){$type="abundant";}
	print $s1 "$group,$group_display,$relab,$store_sorensen{$group},$store_schao{$group},$type\n";
}
close $s1;



open my $s2,">",$out_file_2;
print $s2 "Group,Sample,Type,OTUs,RelAb,Schao,Shannon\n";
foreach my $group (sort { $order{$a} <=> $order{$b} } @tab_groups){
	foreach my $sample(@tab_samples){
		my @t2=split("_",$sample);
		my $type="MES";
		if ($t2[1] eq "DCM" || $t2[1] eq "SUR" || $t2[1] eq "MIX" || $t2[1] eq "dDCM"){$type="EPI";}
		print "$group - $sample - $type\n";
		if ($type eq "EPI"){
			my $group_display=$group;
			if ($keep_group{$group}==1){}
			else{$group_display="Other";}
			if (!defined($store_metrics{$group}{$sample}{"nb_otus"})){
				print $s2 "$group_display,$sample,$type,0,0,0,0\n";
			}
			else{
				print $s2 "$group_display,$sample,$type,$store_metrics{$group}{$sample}{nb_otus},$store_metrics{$group}{$sample}{rel_ab},$store_metrics{$group}{$sample}{Schao},$store_metrics{$group}{$sample}{Shannon}\n";
			}
		}
	}
}
close $s2;