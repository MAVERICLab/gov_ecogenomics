#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to summarize the host prediction by viral contig
# Argument 0 : toto\n";
	die "\n"
}


my $list_population="../List_viral_populations_v3_noBAT.tsv";

my $blastn_file="Host_link_from_blastn.tsv";
my $crispr_file="Contig_matches_from_CRISPR_0.csv";
my $tetra_file="Best_distance_tetra.tsv";

my $out_file="Host_prediction_by_sequence.tsv";
my $out_file_2="Host_prediction_by_sequence-class-filtered-nr.tsv";


my %list_change_phylum=("Proteobacteria"=>1);


my %check_contig;
open my $li,"<",$list_population;
while(<$li>){
	chomp($_);
	my @tab=split("\t",$_);
	my @t2=split(";",$tab[1]);
	foreach(@t2){
		$check_contig{$_}=1;
	}
}
close $li;

# take blast predictoins
my %store;
my $type="Blastn";
print "Reading blastn\n";
open my $tsv,"<",$blastn_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$tab[2]=~s/;\s+/;/g;
	$tab[2]=~s/LUCA;//g;
	my @t2=split(";",$tab[2]);
	if ($t2[2] ne "" && $t2[0] ne "Eukaryota"){
		$store{$tab[0]}{$type}{$tab[1]}{"affi"}=$tab[2];
		my @t3=split("-",$tab[3]);
		$store{$tab[0]}{$type}{$tab[1]}{"strength"}=$t3[0];
	}
}
close $tsv;

print "Reading CRISPR\n";
$type="CRISPR";
open my $csv,"<",$crispr_file;
while(<$csv>){
	chomp($_);
	if ($_=~/^Mic contig/){}
	else{
		my @tab=split(",",$_);
		$tab[4]=~s/;\s+/;/g;
		$tab[4]=~s/LUCA;//g;
		my $type_c=$type."_".$tab[3];
		my @t2=split(";",$tab[4]);
		if ($t2[2] ne "" && $t2[0] ne "Eukaryota"){ # If we have an affi at least at the 3rd level, and we are not in the Eukaryota realm (bad LCA)
# 			print "$tab[1] - $tab[0]\n";
			$store{$tab[1]}{$type_c}{$tab[0]}{"affi"}=$tab[4];
			$store{$tab[1]}{$type_c}{$tab[0]}{"strength"}=$tab[3];
		}
	}
}
close $csv;

$type="Tetra";
print "Reading best tetra distances\n";
open my $tsv,"<",$tetra_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$tab[2]=~s/;\s+/;/g;
	$tab[2]=~s/LUCA;//g;
	my @t2=split(";",$tab[2]);
	if ($t2[2] ne "" && $t2[0] ne "Eukaryota"){
		$store{$tab[0]}{$type}{$tab[1]}{"affi"}=$tab[2];
		$store{$tab[0]}{$type}{$tab[1]}{"strength"}=$tab[3];
	}
}
close $tsv;

my $th_great=0.0001;
my %count;
open my $s1,">",$out_file;
open my $s2,">",$out_file_2;
my %store_temp;
my %store_temp_final;
my $i=1; # Level to be modified if sometimes we don't want the class (e.g  Cyano ?)
my $c=0;
foreach(sort keys %store){
	my $seq=$_;
	if ($check_contig{$seq}!=1){next;}
	print "Processing $seq\n";
	%store_temp=();
	%store_temp_final=();
	$i=1;
	$c=0;
	# First parse the predictions through BLAST
	if (defined($store{$seq}{"Blastn"})){
		print "We have Blastn\n";
		foreach(sort keys %{$store{$seq}{"Blastn"}}){
			$i=1;
			print $s1 "$seq\tblastn\t$store{$seq}{Blastn}{$_}{affi}\t$store{$seq}{Blastn}{$_}{strength}\n";
			my @tab_taxo=split(";",$store{$seq}{"Blastn"}{$_}{"affi"});
			if ($list_change_phylum{$tab_taxo[$i]}==1){$i=2;}
			$store_temp{"blastn"}{$tab_taxo[$i]}++;
			print "\tmatch to $tab_taxo[$i] ($store{$seq}{Blastn}{$_}{affi})\n";
		}
		my @tab_pred=sort {$store_temp{"blastn"}{$a} <=> $store_temp{"blastn"}{$b} } keys %{$store_temp{"blastn"}};
		if ($#tab_pred==0){
			print "\tAll match to the same class -> All good\n";
			print $s2 "$seq\tblastn\t$tab_pred[0]\n";
			$count{"blastn"}++;
			$store_temp_final{$tab_pred[0]}++;
			$c++;
		}
		else{
			print "!!\n";
			foreach(@tab_pred){
				print "$_\t$store_temp{blastn}{$_}\n";
			}
			print "What do we do now ?\n";
# 			<STDIN>;
		}
		
	}
	else{
		print "\tno blastn prediction\n";
	}
	# Then the CRISPR 0
	if (defined($store{$seq}{"CRISPR_0"})){
		print "We have CRISPR_0\n";
		foreach(sort keys %{$store{$seq}{"CRISPR_0"}}){
			$i=1;
			print $s1 "$seq\tCRISPR\t$store{$seq}{CRISPR_0}{$_}{affi}\t$store{$seq}{CRISPR_0}{$_}{strength}\n";
			my @tab_taxo=split(";",$store{$seq}{"CRISPR_0"}{$_}{"affi"});
			if ($list_change_phylum{$tab_taxo[$i]}==1){$i=2;}
			$store_temp{"CRISPR_0"}{$tab_taxo[$i]}++;
			print "\tmatch to $tab_taxo[$i] ($store{$seq}{CRISPR_0}{$_}{affi})\n";
		}
		my @tab_pred=sort {$store_temp{"CRISPR_0"}{$a} <=> $store_temp{"blastn"}{$b} } keys %{$store_temp{"CRISPR_0"}};
		if ($#tab_pred==0){
			print "\tAll match to the same class -> All good\n";
			print $s2 "$seq\tCRISPR_0\t$tab_pred[0]\n";
			$count{"CRISPR_0"}++;
			$store_temp_final{$tab_pred[0]}++;
			$c++;
		}
		else{
			print "!!\n";
			foreach(@tab_pred){
				print "$_\t$store_temp{CRISPR_0}{$_}\n";
			}
			print "What do we do now ?\n";
		}
	}
	elsif (defined($store{$seq}{"CRISPR_1"})){
		print "We have CRISPR_1\n";
		foreach(sort keys %{$store{$seq}{"CRISPR_1"}}){
			$i=1;
			print $s1 "$seq\tCRISPR\t$store{$seq}{CRISPR_1}{$_}{affi}\t$store{$seq}{CRISPR_1}{$_}{strength}\n";
			my @tab_taxo=split(";",$store{$seq}{"CRISPR_1"}{$_}{"affi"});
			if ($list_change_phylum{$tab_taxo[$i]}==1){$i=2;}
			$store_temp{"CRISPR_1"}{$tab_taxo[$i]}++;
			print "\tmatch to $tab_taxo[$i] ($store{$seq}{CRISPR_1}{$_}{affi})\n";
		}
		my @tab_pred=sort {$store_temp{"CRISPR_1"}{$a} <=> $store_temp{"blastn"}{$b} } keys %{$store_temp{"CRISPR_1"}};
		if ($#tab_pred==0){
			print "\tAll match to the same class -> All good\n";
			print $s2 "$seq\tCRISPR_1\t$tab_pred[0]\n";
			$count{"CRISPR_1"}++;
			$store_temp_final{$tab_pred[0]}++;
			$c++;
		}
		else{
			print "!!\n";
			foreach(@tab_pred){
				print "$_\t$store_temp{CRISPR_1}{$_}\n";
			}
			print "What do we do now ?\n";
		}
	}
	else{
		print "\tno CRISPR at all\n";
	}
	# Then the tetra
	if (defined($store{$seq}{"Tetra"})){
		print "We have Tetra\n";
		$i=1;
		my @tab_pred= keys %{$store{$seq}{"Tetra"}};
		if ($#tab_pred>0){
			print "!!! We have $#tab_pred in this table, when we should just have one (so shouhld be 0) !!!\n";
			<STDIN>;
		}
		print $s1 "$seq\tTetra\t$store{$seq}{Tetra}{$tab_pred[0]}{affi}\t$store{$seq}{Tetra}{$tab_pred[0]}{strength}\n";
		my @tab_taxo=split(";",$store{$seq}{"Tetra"}{$tab_pred[0]}{"affi"});
		if ($list_change_phylum{$tab_taxo[$i]}==1){$i=2;}
		$store_temp{"Tetra"}{$tab_taxo[$i]}++;
		print "\tmatch to $tab_taxo[$i] ($store{$seq}{Tetra}{$tab_pred[0]}{affi})\n";
		if ($store{$seq}{"Tetra"}{$tab_pred[0]}{"strength"}<$th_great){
			print $s2 "$seq\tTetra_4\t$tab_taxo[$i]\n";
			$count{"Tetra_great"}++;
			$store_temp_final{$tab_taxo[$i]}++;
			$c++;
		}
		else{
			print $s2 "$seq\tTetra_3\t$tab_taxo[$i]\n";
			$count{"Tetra_good"}++;
			$store_temp_final{$tab_taxo[$i]}++;
			$c++;
		}
	}
	else{
		print "\tno Tetra at all\n";
	}
	my @tab_final=keys %store_temp_final;
	if ($c>0){
		if ($#tab_final<=0 && $c>0){$count{"consistent"}++;} # All good
		else{
			print "!!!!!!!! Weird inconsistent signal\n";
			$count{"inconsistent"}++;
		}
		my $number="Predictions_".$c;
		$count{"total"}++;
		$count{$number}++;
	}
}
close $s1;
close $s2;

foreach(keys %count){
	print "$_\t$count{$_}\n";
}