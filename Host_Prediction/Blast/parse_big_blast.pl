#!/usr/bin/perl
use strict;
use autodie;
# Script to parse the BLAST of GOV contigs against Tara microbial metagenomes
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to parse the BLAST of GOV contigs against Tara microbial metagenomes and list the putative interesting microbial contigs
# Argument 0 : blast file of viral contigs vs microbial metagenome contigs
\n";
	die "\n";
}

my $blast_file=$ARGV[0];
my $fna_file="Tara243_Microbial_metaG_5kb.fna";
my $out_file="Putative_interesting_microbial_contigs.csv";
my $out_file_2="Putative_viral-identical_microbial_contigs.csv";

my $list_population="List_viral_populations_v3.tsv";

my %check_contig;
open my $li,"<",$list_population;
while(<$li>){
	chomp($_);
	my @tab=split("\t",$_);
	my @t2=split(";",$tab[1]);
	foreach(@t2){
		$check_contig{$_}=1;
	}
}
close $li;

my %store_len;
my $c_c="";
print "getting microbial sequence length\n";
open my $fna,"<",$fna_file;
while(<$fna>){
	chomp($_);
	if ($_=~/^>(\S+)/){$c_c=$1;}
	else{$store_len{$c_c}+=length($_);}
}
close $fna;


my $th_id=70;
my $th_score=50;
my $th_evalue=0.001;

my %store_coverage;
print "processing $blast_file ... \n";
open my $tab_p,"<",$blast_file;
while(<$tab_p>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[11]>=$th_score && $tab[10]<=$th_evalue && $tab[2]>=$th_id && $check_contig{$tab[0]}==1){
		if($tab[9]<$tab[8]){
			my $tmp=$tab[8];
			$tab[8]=$tab[9];
			$tab[9]=$tmp;
		}
		$store_coverage{$tab[0]}{$tab[1]}+=$tab[9]-$tab[8];
	}

}
close $tab_p;

my $th_length_hit=2500;
my $th_cover_high=0.90;
my %store_keep;
my %store_id;
foreach(sort keys %store_coverage){
	my $seq_v=$_;
	foreach(sort keys %{$store_coverage{$seq_v}}){
		my $seq_m=$_;
		if (!defined($store_len{$seq_m}) || $store_len{$seq_m}==0){
			print "!!! No length for $seq_m ????\n";
			<STDIN>;
		}
		my $cover=$store_coverage{$seq_v}{$seq_m}/$store_len{$seq_m};
		if ($store_coverage{$seq_v}{$seq_m}>=$th_length_hit){
			print "we have a hit between $seq_v and $seq_m on $store_coverage{$seq_v}{$seq_m}bp";
			if ($cover>=$th_cover_high){
				print " but it seems all viral (>90% coverage)\n";
				$store_id{$seq_m}{$seq_v}=$cover;
			}
			else{
				print " and coverage is 'only' of $cover, we keep\n";
				$store_keep{$seq_m}{$seq_v}=$cover;
			}
		}
	}
}

my %check_viral;
open my $s2,">",$out_file_2;
foreach(sort keys %store_id){
	my $seq_m=$_;
	my $l=$seq_m.",".$store_len{$seq_m}.",";
	$check_viral{$seq_m}=1;
	foreach(keys %{$store_id{$seq_m}}){
		$l.=$_."|".$store_coverage{$_}{$seq_m}."|".$store_id{$seq_m}{$_}.";";
	}
	chop($l);
	print $s2 "$l\n";
}
close $s2;

open my $s1,">",$out_file;
foreach(sort keys %store_keep){
	my $seq_m=$_;
	if ($check_viral{$seq_m}==1){next;} # We remove all data wich matches to microbial contigs that also match a virome contig > 90% coverage
	my $l=$seq_m.",".$store_len{$seq_m}.",";
	foreach(keys %{$store_keep{$seq_m}}){
		$l.=$_."|".$store_coverage{$_}{$seq_m}."|".$store_keep{$seq_m}{$_}.";";
	}
	chop($l);
	print $s1 "$l\n";
}
close $s1;


