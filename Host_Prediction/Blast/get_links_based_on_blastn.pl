#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to get links based on blastn matches
# Argument 0 : toto\n";
	die "\n"
}

my $input_file="Putative_interesting_microbial_contigs.csv";
my $taxo_file="../Databases/Tara243_GORGeCat.contigs.annot_lca.selected_contigs.tsv";
my $out_file="Host_link_from_blastn.tsv";


# First take the list of interesting microbial contigs
print "Reading list of interesting microbial contigs\n";
my %check;
open my $csv,"<",$input_file;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	$check{$tab[0]}=1;
}
close $csv;


# Then take their affi
print "Reading microbial contigs affiliation\n";
open my $tsv,"<",$taxo_file;
my %taxo;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($check{$tab[0]}==1){
		$check{$tab[0]}=2;
		$taxo{$tab[0]}=$tab[1];
	}
}
close $tsv;


# Then re-read the first file to link a viral population to a putative host
print "Making links\n";
open my $s1,">",$out_file;
open my $csv,"<",$input_file;
my %count;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($taxo{$tab[0]} ne ""){
		my @t=split(";",$tab[2]);
		foreach(@t){
			my @t2=split(/\|/,$_);
# 			print "$t2[0] is linked to $taxo{$tab[0]} ($tab[0])\n";
			print $s1 "$t2[0]\t$tab[0]\t$taxo{$tab[0]}\t$t2[1]-$t2[2]\n";
			$count{$t2[0]}++;
		}
	}
}
close $csv;
close $s1;

my $n=0;
foreach(keys %count){
	$n++;
}


my $n_yes=0;
my $n_no=0;

# And check which affi we miss
foreach(keys %check){
	if ($check{$_}==1){
# 		print "no affi for $_\n";
		$n_no++;
	}
	else{
		$n_yes++;
	}
}

print "$n viral contigs linked to a putative host through this BLASTn\n";
print "$n_yes microbial contig with an affi, $n_no without\n";