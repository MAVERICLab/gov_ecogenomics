#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to get links based on CRISPR
# Argument 0 : Th mismatch\n";
	die "\n"
}


my $th_mismatch=$ARGV[0];
my $list_repeats_to_remove="List_CRISPR_low-complexity";
my $list_viral_contigs_in_microbiome="Putative_viral-identical_microbial_contigs.csv";
my $list_viral_matches_in_microbiome="Putative_interesting_microbial_contigs.csv";
my $taxo_microbial_metaG="../Databases/Tara243_GORGeCat.contigs.annot_lca.selected_contigs.tsv";
my $fasta_spacers="CRISPR/CRISPR_spacers_microbial_contigs.fna";
my $input_file="CRISPR/Matches_microbial_CRISPR_to_viral_contigs.tsv";

my $list_population="../List_viral_populations_v3.tsv";

my %check_contig;
open my $li,"<",$list_population;
while(<$li>){
	chomp($_);
	my @tab=split("\t",$_);
	my @t2=split(";",$tab[1]);
	foreach(@t2){
		$check_contig{$_}=1;
	}
}
close $li;


my %crispr_seq;
my $id_c="";
open my $fa,"<",$fasta_spacers;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$id_c=$1;
	}
	else{
		$crispr_seq{$id_c}.=$_;
	}
}
close $fa;


# CRISPR that we don't count because these are low-complexity repeats found everywhere
my %remove_crispr;
open my $li,"<",$list_repeats_to_remove;
while(<$li>){
	chomp($_);
	$remove_crispr{$_}=1;
}
close $li;

# READ THE FILE THAT LIST ALL THE CONTIGS NEARLY IDENTICAL BETWEEN MICROBIOME AND VIROMES
my %remove_matches;
open my $csv,"<",$list_viral_contigs_in_microbiome;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	my @t=split(";",$tab[2]);
	foreach(@t){
		my @t1=split(/\|/,$_);
		$remove_matches{$tab[0]}{$t1[0]}=1;
# 		print "$tab[0] - $t1[0]\n";	
	}
# 	<STDIN>;
	
}
close $csv;

# AND THE FILE WITH LARGE ZONE OF SIMILARITY (i.e. integrated prophages)
open my $csv,"<",$list_viral_matches_in_microbiome;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	my @t=split(";",$tab[2]);
	foreach(@t){
		my @t1=split(/\|/,$_);
		$remove_matches{$tab[0]}{$t1[0]}=1;
# 		print "$tab[0] - $t1[0]\n";	
	}
# 	<STDIN>;
}
close $csv;
# <STDIN>;

open my $tsv,"<",$input_file;
my %crispr_to_contig;
my %count_by_crispr;
my %length_by_crispr;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($remove_crispr{$tab[0]}==1){next;}
	$tab[0]=~/(.*)_CRISPR_\d+_spacer_\d+/;
	my $contig=$1;
# 	print "$tab[0] - $contig\n";
	if ($remove_matches{$contig}{$tab[1]}==1){
# 		print "We remove $_ because we have a big match between these contigs\n";
		next;
	}
	if ($tab[2]<=$th_mismatch){
# 		print "$_\n";
		if ($check_contig{$tab[1]}==1){
			$crispr_to_contig{$tab[0]}{$tab[1]}{$tab[2]}++;
			$length_by_crispr{$tab[0]}=length($tab[5]);
			$count_by_crispr{$tab[0]}++;
		}
	}
}
close $tsv;
print "File $input_file read\n";
# <STDIN>;

my %contig_to_contig;
my $out_file_1="CRISPR_matches_".$th_mismatch.".csv";
open my $s1,">",$out_file_1;
print $s1 "Crispr,length,# matches,list of matches\n";
foreach(sort { $count_by_crispr{$b} <=> $count_by_crispr{$a} } keys %crispr_to_contig){
	my $crispr=$_;
	$crispr=~/(.*)_CRISPR_\d+_spacer_\d+/;
	my $contig=$1;
	my $l=$crispr.",".$length_by_crispr{$crispr}.",".$count_by_crispr{$crispr}.",";
	foreach(sort {$crispr_to_contig{$crispr}{$b} <=> $crispr_to_contig{$crispr}{$a} } keys %{$crispr_to_contig{$crispr}}){
		my $v=$_;
		my $n_1=0;
		foreach(keys %{$crispr_to_contig{$crispr}{$v}}){
			$contig_to_contig{$contig}{$v}{$_}++;
			$n_1++;
		}
		$l.="$_ ($n_1) ";
	}
	chop($l);
	print $s1 "$l\n";
	if ($length_by_crispr{$crispr}<=25 && $count_by_crispr{$crispr}>=5){
		print "$crispr - $length_by_crispr{$crispr} - $count_by_crispr{$crispr} might be a low complexity repeat thing -> $crispr_seq{$crispr}\n";
	}
}
close $s1;

# Taking Tara microbial metaG affiliation
my %affi;
open my $tsv,"<",$taxo_microbial_metaG;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if (defined($contig_to_contig{$tab[0]})){
		$affi{$tab[0]}=$tab[1];
	}
}
close $tsv;
print "affi taken\n";

my $out_file_2="Contig_matches_from_CRISPR_".$th_mismatch.".csv";
open my $s2,">",$out_file_2;
print $s2 "Mic contig,Vir contig,# diff CRISPR matches,# mismatches,Mic contig LCA affi\n";
foreach(keys %contig_to_contig){
	my $mic=$_;
	foreach(keys %{$contig_to_contig{$mic}}){
		my $v=$_;
		foreach(keys %{$contig_to_contig{$mic}{$v}}){
			print $s2 "$mic,$v,$contig_to_contig{$mic}{$v}{$_},$_,$affi{$mic}\n";
		}
	}
}
close $s2;