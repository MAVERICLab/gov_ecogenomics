#!/usr/bin/perl
use strict;
use autodie;
# Script to map back the CRISPR spacer sequences to the viral contigs
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to map back the CRISPR spacer sequences to the viral contigs
# Argument 0 : toto\n";
	die "\n";
}


my $mummer_dir="./Utiles/MUMmer3.23_large_genomes/MUMmer3.23/";
my $fuzznuc_path="fuzznuc";

my $fasta_in="CRISPR_spacers_microbial_contigs.fna";
my $fasta_viral_contigs="../Viral_populations_contigs.fna";
my $sample="Mic_CRISPR";
my $temp_out="temp_fuz";

my $out_file="Matches_microbial_CRISPR_to_viral_contigs.tsv";

my $id=$_;
# map spacer to the microbe contigs
my %store_seq;
my $id_c="";
open my $fa,"<",$fasta_in;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(\S+)/){$id_c=$1;}
	else{$store_seq{$id_c}.=$_;}
}
close $fa;

open my $s1,">",$out_file;
foreach(keys %store_seq){
	my $id_c=$_;
	my $l=length($store_seq{$id_c});
	my $th=int($l/5);
	if ($th>10){$th=10;}
	print "Processing $id_c - $th\n";
	my $cmd="$fuzznuc_path -sequence $fasta_viral_contigs -outfile $temp_out -pattern $store_seq{$_} -pmismatch $th -complement T";
	print "$cmd\n";
	`$cmd`;
	open my $ff,"<",$temp_out;
	my $c_c="";
	my $tag=0;
	while(<$ff>){
		chomp($_);
		if ($_=~/^# Sequence: (\S+)/){$c_c=$1;}
		elsif($_=~/^#/){$tag=0;}
		elsif($_=~/  Start     End/){$tag=1;}
		elsif($tag==1){
			my @tab=split(" ",$_);
			if ($tab[5] eq ""){}
			else{
				if ($tab[4] eq "."){$tab[4]=0;}
				print $s1 "$id_c\t$c_c\t$tab[4]\t$tab[0]\t$tab[1]\t$tab[5]\n";
# 				print "Match de $id_c on $c_c from $tab[0] to $tab[1] (strand $tab[2]) with $tab[4] mismatches (sequence $tab[5])\n";
			}
		}
	}
	close $ff;
# 	<STDIN>;
}
close $s1;
