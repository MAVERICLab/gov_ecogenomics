#!/usr/bin/perl
use strict;
use autodie;
use Getopt::Long;
use Math::CDF  qw(:all);
# use Statistics::ChiSquare;
my $h='';
GetOptions ('help' => \$h, 'h' => \$h);
if ($h==1 || $ARGV[0] eq ""){ # If asked for help or did not set up any argument
	print "# Script to try to compute the significativity of each VC-Host phylum link
# Arguments 0 : toto\n";
	die "\n";	
}

my $cluster_file="Basic_data_GOV_clusters.csv";
my $seq_to_cluster_file="vContact_GOV/cc_sig1.0_mcl2.0_clusters.tsv";
my $db_composition="Host_database_composition.csv";
my $prediction_file="Host_prediction_by_sequence-class-filtered-nr.tsv";


# First get the number of sequences from GOV in each cluster
my %nb_gov;
open my $csv,"<",$cluster_file;
while(<$csv>){
	chomp($_);
	if ($_=~/^Cluster,#/){next;}
	my @tab=split(",",$_);
	$nb_gov{$tab[0]}=$tab[3];
}
close $csv;

my %seq_to_cluster;
my %pcent_cluster;
my $total_cluster=0;
# And the link from sequence to cluster
open my $tsv,"<",$seq_to_cluster_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	my @t1=split(",",$tab[1]);
	foreach(@t1){
		$seq_to_cluster{$_}=$tab[0];
		$pcent_cluster{$tab[0]}++;
		$total_cluster++;
	}
}
close $tsv;
foreach(keys %pcent_cluster){
	$pcent_cluster{$_}/=$total_cluster;
}
# 
# Then the total number of sequences in the host database by host group (csv file listing the number of sequence per host group)
my %pcent_host;
my %count_host;
my $total_hosts;
open my $csv,"<",$db_composition;
while(<$csv>){
	chomp($_);
	if ($_=~/^##/){next;}
	my @tab=split(",",$_);
	$pcent_host{$tab[0]}=$tab[3];
	$count_host{$tab[0]}=$tab[3];
	$total_hosts+=$tab[3];
}
close $csv;
# And transform into percentages
foreach(keys %pcent_host){$pcent_host{$_}/=$total_hosts;}

# Then take predictions
my %cluster_to_pred;
my $total_pred=0;
open my $tsv,"<",$prediction_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if (defined($seq_to_cluster{$tab[0]})){
		$cluster_to_pred{$seq_to_cluster{$tab[0]}}{$tab[2]}++;
# 		if ($seq_to_cluster{$tab[0]} eq "Cluster_0000"){print "$_\n";}
		$total_pred++;
	}
}
close $tsv;

# Compute the general probability to get a prediction if I am a GOV sequence
my $p_pred=$total_pred/($total_cluster*$total_hosts*3);
print "Global prediction rate : $p_pred\n";
# Foreach cluster
foreach my $vc (sort keys %cluster_to_pred){
	# Foreach prediction
	foreach(sort keys %{$cluster_to_pred{$vc}}){
		my $host=$_;
		my $n_pred=$cluster_to_pred{$vc}{$host};
		# Now trying to get the number of expected value, based on the number of trials (number of host of this group, number of sequence GOV in this VC, *3 because we have 3 methods possible to make this link 
		my $n_trials=$count_host{$host}*$nb_gov{$vc}*3;
		# We use the global prediction rate to compute this expected value
		my $e=$p_pred*$n_trials;
		# Based on this, what was the probability to see $n_pred or more (up to $nb_gov{$vc}*3) events if we use a Poisson law of parameter $e to model this rare event process ?
		# This is calculated by substracting the probability to observe less than $n_pred events to 1
		my $poisson=1-ppois($n_pred-1, $e);
		print "$vc\t$host\t$n_pred\t$n_trials\t$e\t$poisson\n";
	}
}