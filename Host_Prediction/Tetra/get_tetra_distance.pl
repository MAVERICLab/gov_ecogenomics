#!/usr/bin/perl
use strict;
use autodie;
use lib '/home/simroux/Utiles/Perl_Modules/';
use Parallel::ForkManager;
# Script to get the distances between all phage sequences and all hosts kmer frequencies with python and a multi-thread approach
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[2])))
{
	print "# Script to get the distances between all phage sequences and all hosts kmer frequencies
# Argument 0 : csv file of hosts
# Argument 1 : csv file of viruses
# Argument 2 : out file\n";
	die "\n";
}

my $host_file_global=$ARGV[0];
my $viral_file=$ARGV[1];
my $out_file_global=$ARGV[2];

my $python_script="get_distance_tetra.py";

# First split the host file in multiple pools
my $limit=5000;
my @tab_hosts;

my $s1;
my $k=0;
my $i=0;
my $out_file="Pool_host_".$k.".csv";
push (@tab_hosts,$out_file);
open $s1,">",$out_file;
open my $csv,"<",$host_file_global;
while(<$csv>){
	$i++;
	if ($i % $limit == 0){
		close $s1;
		$k++;
		my $out_file="Pool_host_".$k.".csv";
		push (@tab_hosts,$out_file);
		open $s1,">",$out_file;
		$i=0;
	}
	print $s1 "$_";
	my @tab=split(",",$_);
	my $t=0;
	for (my $i=1;$i<=$#tab;$i++){$t+=$tab[$i];}
	$t=sprintf("%.02f",$t);
	if ($t!=1.00){print "!! $t - $tab[0]\n";}
}
close $csv;
close $s1;
print "$k pools generated\n";
<STDIN>;
my @tab_out_file;
my $n_processes = 22;
my $pm = Parallel::ForkManager->new( $n_processes );
foreach(@tab_hosts){
	my $host_file=$_;
	$host_file=~/Pool_host_(\d+)\.csv/;
	my $pool=$1;
	my $out_file="Out_pool_".$1.".tsv";
	push(@tab_out_file,$out_file);
	print "Processing $host_file -> $out_file\n";
	$pm->start and next; 
	my $out=`python $python_script -v $viral_file -m $host_file -o $out_file`;
	$pm->finish;
}
$pm->wait_all_children;

my %store_relevant_d;
foreach(@tab_out_file){
	my $file=$_;
	open my $tsv,"<",$file;
	while($tsv){
		chomp($_);
		my @tab=split("\t",$_);
		$store_relevant_d{$tab[0]}{$tab[1]}=$tab[2];
	}
	close $tsv;
}

open my $s1,">",$out_file_global;
foreach(sort keys %store_relevant_d){
	my $vir=$_;
	foreach(sort {$store_relevant_d{$vir}{$a} <=> $store_relevant_d{$vir}{$b} } keys %{$store_relevant_d{$vir}}){
		print $s1 "$vir\t$_\t$store_relevant_d{$vir}{$_}\n";
	}
}
close $s1;