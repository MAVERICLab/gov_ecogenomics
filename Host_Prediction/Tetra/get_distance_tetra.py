import os
import argparse
import pandas
from itertools import *
import math

def std_sum(v1, v2):
	distance = 0.0
	for (a,b) in izip(v1, v2):
		distance += math.fabs(a-b)
	distance/=len(v1)
	return distance


def main():
	#path_root = "/home/data1/simroux/GOV_analysis/Host_link/Tetra"
	#os.chdir(path_root)
	parser = argparse.ArgumentParser(description='To get the correc files.')
	parser.add_argument('-v','--virus_file', type=str, help='path to the viral file')
	parser.add_argument('-m','--microbe_file', type=str, help='path to the microbial file')
	parser.add_argument('-o','--out_file', type=str, help='path to the output file')
	args=parser.parse_args();
	f = open(args.out_file,'w')
	th_d=0.001
	# Load vectors
	viral_vector = pandas.read_csv(args.virus_file,header=None, index_col=0)
	#print (viral_vector)
	microbial_vector = pandas.read_csv(args.microbe_file,header=None, index_col=0)
	#print (microbial_vector)
	for index_1, row_1 in viral_vector.iterrows():
		#print "Processing ", index_1
		for index_2,row_2 in microbial_vector.iterrows():
			d=std_sum(row_1,row_2)
			#print index_1," vs ",index_2+" gives a distance of ",d
			if d <= th_d:
				f.write(index_1+"\t"+index_2+"\t"+"{:.9f}".format(d)+"\n")
	f.close()

if __name__ == "__main__":
    output = main()