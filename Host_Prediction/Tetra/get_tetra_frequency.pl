#!/usr/bin/perl
use strict;
use autodie;
# Script to get the tetra frequency of a set of sequences
# Argument 0 : fasta input
# Argument 1 : output
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[1])))
{
	print "# Script to get the tetra frequency of a set of sequences
# Argument 0 : fasta input
# Argument 1 : output\n";
	die "\n";
}

my $path_to_jellyfish="./Utiles/Jellyfish/jellyfish-2.1.3/bin/jellyfish";
my $fasta_in=$ARGV[0];
my $tetra_freq=$ARGV[1];

my $id_c="";
my $seq_c="";
my $out="";
if (-e $tetra_freq){`rm $tetra_freq`;}
open my $fa,"<",$fasta_in;
my $temp_out="temp_out_v";
my $temp_kmer="temp_kmer_v";
my $temp_dump="temp_dump_v";
my $tag=0;
while(<$fa>){
	chomp($_);
	if($_=~/^>(\S+)/){
		my $id=$1;
		$tag=0;
		if($id_c ne ""){
			open my $s1,">",$temp_out;
			print $s1 ">$id_c\n$seq_c\n";
			close $s1;
			$out=`$path_to_jellyfish count -m 4 -s 10M -t 8 -C $temp_out -o $temp_kmer`;
			print "$out\n";
			print "dumping result ...";
			$out=`$path_to_jellyfish dump -c $temp_kmer > $temp_dump`;
			$out=`echo ">$id_c" >> $tetra_freq`;
			$out=`cat $temp_dump >> $tetra_freq`;
		}
		$id_c=$id;
		$seq_c="";
	}
	else{
		$seq_c.="$_";
	}
	
}
if($id_c ne ""){
	$out=`$path_to_jellyfish count -m 4 -s 10M -t 8 -C $temp_out -o $temp_kmer`;
	print "$out\n";
	print "dumping result ...";
	$out=`$path_to_jellyfish dump -c $temp_kmer > $temp_dump`;
	$out=`echo ">$id_c" >> $tetra_freq`;
	$out=`cat $temp_dump >> $tetra_freq`;
}
close $fa;
