#!/usr/bin/perl
use strict;
use autodie;
# Script to convert a fts file in a vector file
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[1])))
{
	print "# Script to convert tetranucleotide frequency files from fts to csv files
# Argument 0 : fts file
# Argument 1 : out file\n";
	die "\n";
}

my $fts_file=$ARGV[0];
my $out_file=$ARGV[1];

print "We get the list of refs ... ";
my $liste_ref="list_refs";
open(LI,"<$liste_ref") || die ("pblm opening file $liste_ref\n");
my %check;
my @tab_tetra;
while(<LI>){
	chomp($_);
	my @tab=split(",",$_);
	foreach(@tab){
		if (length($_)==4){
			$check{$_}=1;
			push(@tab_tetra,$_);
		}
	}
}
close LI;
print "Done\n";

print "We get all values\n";
my %store_value;
my %store_total;
open my $fts,"<",$fts_file;
my $c_c="";
while(<$fts>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$c_c=$1;
		if ($c_c=~/^\d/){
			$c_c="NCBI_".$c_c;
		}
	}
	else{
		my @tab=split(" ",$_);
		if ($check{$tab[0]}==1){
			if (defined($store_value{$c_c}{$tab[0]})){
				print "!!!!!!!!!!!! ALREADY A VALUE FOR $c_c and $tab[0] !!!\n";
				<STDIN>;
			}
			$store_value{$c_c}{$tab[0]}=$tab[1];
			$store_total{$c_c}+=$tab[1];
# 			print "we had $tab[0] in the list\n";
		}
		else{
			my $r=&revcomp($tab[0]);
			if ($check{$r}==1){
				if (defined($store_value{$c_c}{$r})){
					print "!!!!!!!!!!!! ALREADY A VALUE FOR $c_c and $r !!!\n";
					<STDIN>;
				}
				$store_value{$c_c}{$r}=$tab[1];
				$store_total{$c_c}+=$tab[1];
			}
			else{
				print "!!!! $tab[0] / $r ??????\n";
				<STDIN>;
			}
		}
	}
	
}
close $fts;

print "We convert them to vectors\n";
open my $s1,">",$out_file;
foreach(keys %store_value){
	my $seq_c=$_;
	my $l="";
	foreach(@tab_tetra){
		$l.=$store_value{$seq_c}{$_}/$store_total{$seq_c}.",";
	}
	chop($l);
	print $s1 "$seq_c,$l\n";
}
close $s1;


sub revcomp{
	my $seq=$_[0];
# 	print "$seq\n";
	$seq=~tr/atgc/tacg/;
	$seq=~tr/ATCG/TAGC/;
	$seq=reverse($seq);
	return $seq;
}
