#!/usr/bin/perl
use strict;
use autodie;
# Script to parse the Tetra distances of GOV contigs against Tara microbial metagenomes contigs
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to parse the Tetra distances of GOV contigs against Tara microbial metagenomes contigs
# Argument 0 : toto
\n";
	die "\n";
}

my @list_d=<Tetra/Out_file_distances/Out_pool*.tsv>;
my $taxo_file="../Databases/Tara243_GORGeCat.contigs.annot_lca.selected_contigs.tsv";
my $list_viral_contigs="Putative_viral-identical_microbial_contigs.csv";
my $out_file="Best_distance_tetra_Tara_Metagenome.tsv";

print "List viral contigs that will be excluded\n";
open my $csv,"<",$list_viral_contigs;
my %check_viral;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	$check_viral{$tab[0]}=1;
}
close $csv;


print "Read distances\n";
my %check;
my %store;
foreach(@list_d){
	my $file=$_;
	open my $tsv,"<",$file;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		$store{$tab[0]}{$tab[1]}=$tab[2];
		$check{$tab[1]}=1;
	}
	close $tsv;
}


# Then take their affi
print "Reading microbial contigs affiliation\n";
open my $tsv,"<",$taxo_file;
my %taxo;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($check{$tab[0]}==1){
		$check{$tab[0]}=2;
		$taxo{$tab[0]}=$tab[1];
	}
}
close $tsv;

open my $s1,">",$out_file;
my $th=0.0001; # Th at 0.001 already done in the python script, this th is between very good and good predictions 
foreach(sort keys %store){
	my $seq=$_;
	print "looking at seq $seq\n";
	my @tab_host=sort { $store{$seq}{$a} <=> $store{$seq}{$b}  } keys %{$store{$seq}};
	my $tag=0;
	for (my $i=0;$i<=$#tab_host;$i++){
		my $h=$tab_host[$i];
		my $d=$store{$seq}{$h};
		if ($check_viral{$h}==1){
			print "we don't care about $h because it looks like a 100% viral in microbiome\n";
		}
		elsif (defined($taxo{$h})){
			print "\tbest distance -> $d -> $h -> $taxo{$h}";
			my @tab=split(";",$taxo{$h});
			if ($#tab<2){
				print " we don't really care, no real affiliation\n";
			}
			else{
				$tag=1;
				if ($d<=$th){
					print " really good ! \n";
					print $s1 "$seq\t$h\t$taxo{$h}\t$d\t1\n";
				}
				else{
					print " good but not great ! \n";
					print $s1 "$seq\t$h\t$taxo{$h}\t$d\t2\n";
				}
				# Just to check
				if ($d>0.001){
					print "!! We have $d when we should not have anything above 10-3 ?\n";
					<STDIN>;
				}
			}
		}
		else{
			print "\tdistance $i - $h but no taxo ($check{$h}) so we don't check\n";
		}
		if ($tag==1){
			$i=$#tab_host+2;
		}
	}
# 	<STDIN>;
}
close $s1;
